FROM php:8.2-apache

RUN docker-php-ext-install pdo

RUN a2enmod rewrite && service apache2 restart
RUN chown -R www-data:www-data /var/www/html
RUN chmod -R 755 /var/www/html
RUN echo "DocumentRoot /var/www/html" >> /etc/apache2/sites-available/000-default.conf
RUN echo "<Directory /var/www/html>" >> /etc/apache2/sites-available/000-default.conf
RUN echo "  AllowOverride All" >> /etc/apache2/sites-available/000-default.conf
RUN echo "</Directory>" >> /etc/apache2/sites-available/000-default.conf

WORKDIR /var/www/html
