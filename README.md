# Docker TD4

## Instructions :


Pour commencer, on lance le service docker pour l'utilisateur courant :

    systemctl --user start docker-desktop.service

On crée un dossier "docker" pour regrouper nos fichiers de configuration et autres volumes.

Le premier de ces fichiers est le docker-compose.yml, qu'on va créer avec ce contenu :

	version: '3.8'
	services:
	  apache:
	    image: php:8.2-apache
	    container_name: apache_serv
	    depends_on:
	      - postgis
	    volumes:
	      - ./apache/html:/var/www/html
	      - ./apache/sites_available:/etc/apache2/sites_available
	      - ./apache/sites_enabled:/etc/apache2/sites_enabled
	    ports:
	      - 8180:80
	    networks:
	      - my-network-td4

	  postgis:
	    image: postgis/postgis
	    container_name: postgre_serv
	    environment:
	      POSTGRES_USER: tp1
	      POSTGRES_PASSWORD: tp12023
	      POSTGRES_DB: raphael_db
	    volumes:
	      - ./postgis/data:/var/lib/postgresql/data
	      - ./postgis/db/custom.conf:/etc/postgresql/conf.d/custom.conf
	    ports:
	      - 8181:80
	    networks:
	      - my-network-td4

	  pgadmin:
	    image: dpage/pgadmin4
	    container_name: pgadmin_serv
	    depends_on:
	      - postgis
	    environment:
	      PGADMIN_DEFAULT_EMAIL: raphael.izoret@etu.umontpellier.fr
	      PGADMIN_DEFAULT_USER: tp1
	      PGADMIN_DEFAULT_PASSWORD: tp12023
	    ports:
	      - 8182:80
	    networks:
	      - my-network-td4

	  minetest:
	    image: linuxserver/minetest
	    container_name: my_minetest_server
	    ports:
	      - 25565:80
	    networks:
	      - my-network-td4

	networks:
	  my-network-td4:
	    external: false
	    name: my-network-td4


On peut désormais tester notre docker avec la commande

    docker compose up -d

, puis
   
    docker ps

pour vérifier que les conteneurs tournent.

Mais nous n'avons pas fini la configuration.

Dans docker/apache/html, qui est un volume lié au conteneur apache que nous avons créé, on crée un fichier index.php avec un contexte html basique et la commande <?= phpinfo() ?> dans le body.

On va également créer un fichier Dockerfile à la racine du projet pour initialiser le docker avec des commandes :

    FROM php:8.2-apache

    RUN docker-php-ext-install pdo

    RUN a2enmod rewrite && service apache2 restart
    RUN chown -R www-data:www-data /var/www/html
    RUN chmod -R 755 /var/www/html
    RUN echo "DocumentRoot /var/www/html" >> /etc/apache2/sites-available/000-default.conf
    RUN echo "<Directory /var/www/html>" >> /etc/apache2/sites-available/000-default.conf
    RUN echo "  AllowOverride All" >> /etc/apache2/sites-available/000-default.conf
    RUN echo "</Directory>" >> /etc/apache2/sites-available/000-default.conf

    WORKDIR /var/www/html

Désormais, pour initialiser l'ensemble, il suffit de lancer à la racine du projet :

	docker build .
	docker compose up -d

Et les services seront tous crées directement, sur un network bridge visible avec

	docker network ls

sous le nom de my-network-td4 contenant chacun des services.
